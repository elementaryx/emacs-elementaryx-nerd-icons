;;; ElementaryX: Elementary Emacs configuration coupled with Guix
;;;
;;; emacs-nerd-icons setup for:
;;; - emacs-nerd-icons
;;; - emacs-nerd-icons-completion
;;; - emacs-nerd-icons-dired
;;; - emacs-nerd-icons-ibuffer
;;; - emacs-treemacs-nerd-icons
;;; - emacs-spaceline-nerd-icons

;;; Usage: This can be used as a standalone package, independently of
;;; the rest of the elementaryx suite, e.g.:
;;;   `guix shell emacs emacs-elementaryx-nerd-icons`

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Nerd icons
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(use-package nerd-icons
  ;; :custom
  ;; The Nerd Font you want to use in GUI
  ;; "Symbols Nerd Font Mono" is the default and is recommended
  ;; but you can use any other Nerd Font if you want
  ;; (nerd-icons-font-family "Symbols Nerd Font Mono")
  )

;; Nerd icons
;; nerd-icons-dired
;; treemacs-nerd-icons
;; ;; nerd-icons-ivy-rich
;; nerd-icons-ibuffer
;; nerd-icons-completion
;; nerd-icons-corfu
;; dirvish ;; https://github.com/alexluigit/dirvish
;; - prerequisites: https://github.com/alexluigit/dirvish?tab=readme-ov-file#prerequisites
;; - dirvish-side
;; - nerd-icons with dirvish: https://github.com/rainstormstudio/nerd-icons.el?tab=readme-ov-file#use-nerd-icons-with-dirvish
;; doom-modeline
;; nerd-fonts https://github.com/ryanoasis/nerd-fonts

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Mode line: doom-modeline
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; https://www.gnu.org/software/emacs/manual/html_node/emacs/Mode-Line.html

(use-package doom-modeline
  :init (doom-modeline-mode 1)
  :config
  ;; Enable flashing mode-line on errors
  (doom-themes-visual-bell-config)
  ;; Corrects (and improves) org-mode's native fontification.
  (doom-themes-org-config)
  )

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Treemacs: treemacs-nerd-icons
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; (use-package treemacs-all-the-icons
;;   :after treemacs
;;   :config (treemacs-load-theme "all-the-icons"))

(provide 'elementaryx-nerd-icons)
